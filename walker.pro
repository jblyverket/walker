TEMPLATE = lib
TARGET = walker
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp

release {
    QMAKE_CXXFLAGS_RELEASE -= -O2
    QMAKE_CXXFLAGS_RELEASE += -O3
}

COMMON_CXXFLAGS = -std=c++0x -shared -fPIC
QMAKE_CXXFLAGS += $$COMMON_CXXFLAGS
QMAKE_CXXFLAGS_RELEASE += $$COMMON_CXXFLAGS
QMAKE_CXXFLAGS_DEBUG += $$COMMON_CXXFLAGS

INCLUDEPATH += /usr/include/python2.7
