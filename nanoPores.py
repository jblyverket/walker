# -*- coding: utf-8 -*-
"""
Created on Fri May  9 16:03:54 2014

Program for creating a percolation cluster and analyzing this 2D

@author: josteibl
"""

# Imports
import numpy as np
import matplotlib.pylab as plt
from pylab import *
from scipy.ndimage import measurements
from scipy.sparse import spdiags, dia_matrix, coo_matrix
from scipy.sparse.linalg import spsolve
#import libwalker


# Defining variables



p = 0.6586
lx = 150
ly = 150
pc = 0.59275

# Generating a random matrix
z = np.random.rand(lx,ly)<pc

lw,num = measurements.label(z)
 

# ----------------------------
# Finding percolation cluster
# ----------------------------

def percolationCLusterFinder(lw,lx):
    """
    Function locating the percolating cluster
    """
 
     
    percY = np.intersect1d(lw[0,:], lw[lx-1,:])

    if (percY.size > 0 and np.any(percY)):  # Checking the length of the vector
                                            # Checking for vector elements > 0

         maxPercY = np.max(percY)           # Finding max area of perc.clusterS
         zz = lw == maxPercY                # zz only contains perc.Cluster
         
         perClusterArea = np.sum(zz)

         return zz, perClusterArea 
         
    else:
         print "Error, no percolation cluster"  # fix this test  
         return 0
# ----------------------------      
   
   
   
#zz = percolationCLusterFinder(lw,lx)      
#nx = len(zz)
#ny = len(zz)
#
#print libwalker.leftwalk(zz,nx,ny)     
#         
         
# ----------------------------
# Singly connected bonds
# ----------------------------
   
def scb(zz):
    """
    Function for finding the singly connected bonds 
    of the percolating cluster    
    """
    # Create walk first
#    left, right = walk(zz)
#    singlycb = left[:,:]*right[:,:]    
    return  0#singlycb
    
# ----------------------------
    
# ----------------------------
# Backbone
# ----------------------------
    
def backBone(zz):
    """
    Function for finding the backbone
    of the percolating cluster    
    """
    return 0
    
# ----------------------------    
    
# ----------------------------
# Backbone
# ----------------------------
    
def danglingEnds(zz):
    """
    Function for finding the dangling ends
    of the percolating cluster    
    """
    return 0
    
# ----------------------------      
    
    
# -------------------------
# Percolation image
# -------------------------

def plottingFunction(z,lw, percClus,singlycb):
    """
    Function for plotting the various clusters
    Add title and so on later.....
    """
    
    plt.figure()
    plt.imshow(z, interpolation = 'None')
    plt.show()
    plt.figure()
    plt.imshow(lw, interpolation = 'None')
    plt.show()
    plt.figure()
    plt.imshow(percClus, interpolation = 'None')
    plt.show()
    plt.figure()
    plt.imshow(singlycb, interpolation = 'None')
    plt.show()
# -------------------------     
    
# -------------------------
# Bond lattice
# -------------------------    
    
def sitebond(zz):
    
    nx = size(zz,1-1)
    ny = size(zz,2-1)

    N = nx*ny
    
    gg_r = np.zeros((nx , ny))
    gg_d = np.zeros((nx , ny))
    
    gg_r[:,0:ny-1] = zz[:,0:ny-1]*zz[:,1:ny]
    gg_r[:, ny-1] = zz[:, ny - 1]
    gg_d[0:nx-1,:] = zz[0:nx-1,:]*zz[1:nx,:]
    gg_d[nx-1, :] = 0
    
    
    g = np.zeros((nx*ny , 2))
    g[:,0] =  gg_d.reshape(-1, order='F').T
    g[:,1] = gg_r.reshape(-1,order='F').T
    return g
# -------------------------    



# % Written by Marin Soreng
#% ( C ) 2004

def findCond(A, xLength,yLength):
    pIn = 1
    pOut = 0
    
    # Call mkEqsystem
    B,C = mkEqsystem(A,xLength, yLength)    
    
    # Kirchoff's equations, solve for p
    p = spsolve(B, C)
    
    p = np.concatenate((pIn * np.ones(xLength), p, pOut*np.ones(xLength)))
    ceff = dot((p[-1-2*xLength:-1-xLength] - pOut).T, A[-1-2*xLength:-1-xLength, 1])/(pIn - pOut)
#    
    return p, ceff
    

def coltomat(zz, x, y):
    g = np.zeros((x , y))    
    for iy in range(1,y):
        i = (iy - 1) * x + 1
        ii = i + x - 1
        g[: , iy - 1] = zz[ i - 1 : ii]
    return g
    
    
    
    
    
#% Written by Marin S r e n g
#% ( C ) 2004
#% Sets up Kirchoff ’ s equations for the 2 D lattice A .
#% A has X * Y rows and 2 columns . The rows indicate the site ,
#% the first column the bond perpendicular to the flow direction
#% and the second column the bond parallel to the flow direction .
#% The return values are [B , t ] where B * x = C . This is solved178
#% APPENDIX A. COMPUTER CODE    
def mkEqsystem(A, xLength, yLength):
    
    sites = xLength*(yLength - 2) # total number of internal lattice sites
    
    mainDiag = np.zeros(sites)
    upperDiag1 = np.zeros(sites - 1)
    upperDiag2 = np.zeros(sites - xLength)
    
    mainDiag = A[xLength : xLength*(yLength-1),0] + A[xLength : xLength*(yLength-1), 1] 
    + A[0 : xLength*(yLength-2), 1]
    + A[xLength-1 : xLength*(yLength-1)-1, 0]
    upperDiag1 = A[xLength : xLength*(yLength-1)-1, 0]
    upperDiag2 = A[xLength : xLength*(yLength-2), 1]
    mainDiag[where(mainDiag == 0)] = 1
    
    B = dia_matrix((sites , sites))
    B = -spdiags(upperDiag1, -1, sites, sites)
    B = B + -spdiags(upperDiag2, 0, sites, sites)
    B = B + B.T + spdiags(mainDiag, 0, sites, sites)
    C = zeros(sites)
    
    C[0:xLength] = A[0: xLength, 1]
    C[-1-xLength + 1: -1] = 0*A[-1 -2*xLength + 1: -1-xLength, 1]
    
    return B, C
#    
#

 
def main():
    
    percClus, areaPerc = percolationCLusterFinder(lw,lx)    
    
    
#    singlycb = scb(percClus)
    
    zzz = percClus.T    
    
    g = sitebond(zzz)
    
#    plt.figure()
#    plt.imshow(g[:,0].reshape(lx,ly), interpolation='nearest')
#    plt.figure()
#    plt.imshow(g[:,1].reshape(lx,ly), interpolation='nearest')
#    plt.figure()
#    plt.imshow(zzz, interpolation='nearest')
    
    p1, cEff = findCond(g, lx, ly)
    
    x = coltomat(p1, lx, ly)
    P = x*zzz
    
#    plt.figure()
#    plt.imshow(P,  interpolation = 'None')

    g1 = g[:,0]    
    g2 = g[:,1]
    
    z1 = coltomat(g1, lx, ly)
    z2 = coltomat(g2, lx, ly)
    
    f2 = np.zeros((lx , ly))
    
    for iy in range(0,ly-1):
        f2[:,iy] = (P[ :, iy] - P[:,iy+1])*z2[:,iy]
    
    f1 = np.zeros((lx , ly))    
    for ix in range(0,lx-1):
        f1[ix,:] = (P[ix,:] - P[ix+1,:])*z1[ix,:]
        
    fn = np.zeros((lx , ly))    
    fn[:,:] = fn[:,:] + np.abs(f1[:,:])
    fn[:,:] = fn[:,:] + np.abs(f2[:,:])
    
    fn[:, 1:ly] = fn[:,1:ly] + np.abs(f2[:, 0:ly-1])
    fn[:, 0] = fn[:, 0] + np.abs((P[:, 0] - 1.0 )*(zzz[:,0]))
    fn[1:lx :] = fn[1:lx, :] + np.abs(f1[0:lx-1, :])
    zfn = fn
    zbb = (zzz + 2 * zfn)
    zbb = zbb/np.max(np.max(zbb))
    
   
    plt.figure()
    plt.imshow(fn, interpolation='nearest')
    plt.show()
    
    
#    a = plottingFunction(z,lw,percClus, singlycb)  
    
if __name__=="__main__":
   main()    
            














