#include <iostream>
#include <iomanip>
#include <cmath>
#include <cstdlib>
#include <vector>


#include <armadillo>
#include <boost/python.hpp>

using namespace std;
using namespace arma;



vector<vector<int> > leftwalk(int** zz, int nx, int ny) {


//    vector<vector<double> > zz {{1, 0, 0},{1, 1, 0},{0, 1, 0}};

//    vector<vector <double> > left(zz.size(),vector<double>(3,0.0));


    // Checking the size
//    cout << nx << endl;

    // We calculate percolation in the y-direction
    // so checking the first column for sites
    int x0 = 0; // always zero
    int y0;
    for (uint i = 0; i<nx; i++){
        // CHECK IF THIS HOLDS
        if (zz[x0][i] == 1){
            y0 = i;
        }
    }
    cout << "Starting point of walker: " << y0 << endl;


    vector<vector <double> > directions(4, vector<double>(2,0.0));

    directions[0][0] = -1; // west x
    directions[0][1] = 0;  // west y
    directions[1][0] = 0; // south x
    directions[1][1] = -1; // south y
    directions[2][0] = 1; //
    directions[2][1] = 0;  //
    directions[3][0] = 0; //
    directions[3][1] = 1; //

    int nwalk = 1;  // Number of walks

    int ix = x0;
    int iy = y0;
    int iix;
    int iiy;
    int direction = 1;
    int nfound;
    vector<vector <int> > left(nx, vector<int>(ny,0.0));


    while(nwalk > 0){
        left[ix][iy] = left[ix][iy] + 1;
        nfound = 0;

        while(nfound == 0){
            direction = direction - 1;
//            cout << "Direction: " << direction << endl;
            if(direction < 0 ){
                direction = direction + 4;
//                cout << "Direction 2: " << direction << endl;
            }
            iix = ix + directions[direction][0];
            iiy = iy + directions[direction][1];
//            cout << "Size of iix: " << iix << endl;
            if(iix >= nx){
                nwalk = 0;  // Walk is then finished reached the end
//                cout << "Walk is finished" << endl;
                iix = nx;
                int ix1 = ix;
                int iy1 = iy;
            }
            else if(iix >= 0){
                if(iiy >= 0){
                    if(iiy < ny){
                        if(zz[iix][iiy] > 0){
                            ix = iix;
                            iy = iiy;
                            nfound = 1;
                            direction = direction + 2;
//                            cout << "nfound = 1" << endl;
                            if(direction > 3){
                                direction = direction - 4;

                            }
                        }
                    }
                }
            }

        }
    }

    return left;
}

vector<vector<int> > rightwalk(int** zz, int nx, int ny) {


//    vector<vector<double> > zz {{1, 0, 0},{1, 1, 0},{0, 1, 0}};
            // Checking the size
//    cout << nx << endl;

    // We calculate percolation in the y-direction
    // so checking the first column for sites
    int x0 = 0; // always zero
    int y0;
    for (uint i = 0; i<nx; i++){
        // CHECK IF THIS HOLDS
        if (zz[x0][i] == 1){
            y0 = i;
        }
    }
    cout << "Starting point of walker: " << y0 << endl;


    vector<vector <double> > directions(4, vector<double>(2,0.0));

    directions[0][0] = -1; // west x
    directions[0][1] = 0;  // west y
    directions[1][0] = 0; // south x
    directions[1][1] = -1; // south y
    directions[2][0] = 1; //
    directions[2][1] = 0;  //
    directions[3][0] = 0; //
    directions[3][1] = 1; //


    vector<vector <int> > right(nx, vector<int>(ny,0.0));

    int nwalkR = 1;
    int iix;
    int iiy;
    int ixR = x0;
    int iyR = y0;
    int directionR = 1;
    int nfoundR = 0;
    while(nwalkR > 0){
        right[ixR][iyR] = right[ixR][iyR] + 1;
        nfoundR = 0;

        while(nfoundR == 0){
            directionR = directionR + 1;
            if(directionR > 3){
                directionR = directionR -4;


            }
            iix = ixR + directions[directionR][0];
            iiy = iyR + directions[directionR][1];
//            cout << direction << endl;
            if(iix >= nx){
                nwalkR = 0;
                iix = nx;
                int ix1 = ixR;
                int iy1 = iyR;
//                cout << "walker here?" << endl;
            }
            else if(iix >= 0){
                if(iiy >= 0){
                    if(iiy < ny){
                        if(iix < nx){
                            if(zz[iix][iiy] > 0){
                                ixR = iix;
                                iyR = iiy;
                                nfoundR = 1;
//                                cout << "nfound R" << endl;
                                directionR = directionR - 2;
                                if(directionR < 0){
                                    directionR = directionR + 4;

                                }
                            }
                        }
                    }
                }
            }
        }
    }



    for(uint i = 0; i <= nx; i++){
        for(uint j = 0; j <= ny; j++){
            cout << right[i][j] << endl;
        }
    }
    return right;

}

int a() {
    return 2;
}

int c() {
    return 3;
}

BOOST_PYTHON_MODULE(libwalker)
{
    using namespace boost::python;
//    def("leftwalk", leftwalk);
//    def("rightwalk", rightwalk);
    def("a", a);
    def("c", c);
}





























